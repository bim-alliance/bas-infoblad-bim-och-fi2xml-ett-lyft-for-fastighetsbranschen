![Bild 1](media/1.jpeg)

**Så här ser informationsmodellen, med ingående delsystem och intressenter, ut. Fi2XML-språket möjliggör kommunikation mellan olika förvaltningssystem och underlättar åtkomst, hantering och utbyte av information.**

# BIM och Fi2XML ett lyft för fastighetsbranschen

> ##### Fi2XML-språket har inneburit ett lyft för fastighetsbranschen. Tack vare denna standard är det möjligt att kommunicera fastighetsinformation mellan olika förvaltningssystem. Fi2 sparar tid och pengar, både i användnings- och förvaltningsskedet.

REGIONSERVICE INOM REGION SKÅNE HAR använt Fi2XML sedan 2002 och CAD-strateg Per Erlandsson är mycket nöjd. 
​	– Vi gör en fantastisk vinst på att använda Fi2, både tids och arbetsmässigt, säger han. Fi2 underlättar åtkomst, hantering och utbyte av fastighetsinformation. Normalt sett kan en förvaltare behöva mäta en area ett tiotal gånger, här får vi ytan med automatik från CAD-modellerna. Uppgiften är spikad redan från början.
​	Fi2 är en fortsättning på Fi2002, som i sin tur var en del av utvecklingsprojektet Bygg och Fastighet 2002. Fi2-standarden sköts av Föreningen för Förvaltningsinformation, FFI, som stöds av ett antal stora aktörer inom fastighetssektorn. XML är en förkortning av eXtensible Markup Language, en vidareutveckling av HTML – Hyper Text Markup Language. XML-standarden
gör det möjligt att utbyta och publicera information på ett strukturerat sätt.
​	Hittills har Regionservice använt Fi2 för rumsinformation. Här handlar det om cirka hundra tusen rum. Basen i informationssystemet är originalmodeller (BIM) och tekniska beskrivningar, det vill säga utrymmesoriginalen utgörs av BIMmodeller.
Härifrån går information till fastighetsdatabasen Landlord och publiceringsarkivet HyperDoc. Här kan man exempelvis välja modulen Städ. När man läser in olika rum från fastighetsdatabasen får rummen automatiskt städkoder med all information kopplad till städningen. All annan information finns också samlad till det aktuella rummet, till exempel hyra.
​	– Den stora vinsten är att all information kring ett specifikt objekt finns samlad på en plats. Tidigare fanns samma rum på olika platser utifrån om det exempelvis handlade om hyra, om städ eller om kundtjänst. Nu kan vi titta på en objektritning
och få en perfekt avstämning mot fastighetsdatabasen, säger Per Erlandsson och fortsätter:
​	– Det är mycket viktigt att få ordning på areor, uthyrning och städ, vilket ligger till grund för hela förvaltningen av byggnader och fastigheter. Vi har en helt annan koll nu jämfört med tidigare och informationen om vårt fastighetsbestånd är tillgänglig på ett annat sätt. Dessutom följer all fastighetsinformation med fastigheten vid köp och försäljning.
​	Vid ombyggnader och mindre förändringar gör dokumenthandläggare på Regionservice själva ändringar i originalmodellerna. Dessa läses sedan in i systemet med hjälp av Fi2XML. Vid nybyggnation finns krav att allt arbetsunderlag
ska lämnas tillbaka i sådant skick att det kan läsas i Fi2XML. Fi2XML kan översättas till IFC, den internationella standarden för objektorienterade bygginformationsmodeller.
​	Nästa steg i Regionservice användning av Fi2XML blir att inlemma energideklarationer i systemet liksom installationer. Dessa kommer att länkas till respektive rum och får samma data som rummet. Dessutom får varje installation en identitet som gör det möjligt att se om det exempelvis är en radiator
eller ett ventilationsdon.
​	– Användningsområdena är oändliga, säger Per Erlandsson. Allt som ingår och tillhör en fastighet kan finnas med i systemet. Fi2XML är bärare av information när det gäller det mesta, till exempel när det gäller bygglov. Informationen går direkt från fastighetsdatabasen till myndigheterna. Arbetet med att utveckla Fi2XML till att i stort sett omfatta allting sker i samverkan mellan systemleverantörer och FFI. Man måste vara överens om vad man kallar saker och ting. För närvarande finns ett fastighetslexikon med 1 800 termer och till dessa knyts systemnamn i Fi2. Arbetet med att utöka begreppsfloran pågår kontinuerligt. 
​	Användarna deltar i den behovsstyrda utvecklingen. Inom FFI finns intressegrupper för olika saker, till exempel systemleverantörer och leveransspecifikationer, och det är i dessa forum som utvecklingen sker. Denna är med andra ord väl förankrad i den praktiska verkligheten.
​	– Andra länder sägs ha kommit längre än vi men de har inte den detaljnivå som vi har. Där blir det mer ett överbygge som inte riktigt når ner till brukarna.